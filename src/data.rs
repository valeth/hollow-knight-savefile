use serde::{Serialize, Deserialize};
use serde_json as json;

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(deny_unknown_fields, rename_all = "camelCase")]
pub struct SaveData {
    pub player_data: PlayerData,
    pub scene_data: SceneData,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct PlayerData {
    pub at_bench: bool,

    #[serde(flatten)]
    pub extra: json::Value,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(deny_unknown_fields, rename_all = "camelCase")]
pub struct SceneData {
    pub geo_rocks: Vec<GeoRock>,
    pub persistent_bool_items: Vec<PersistentBoolItem>,
    pub persistent_int_items: Vec<PersistentIntItem>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(deny_unknown_fields, rename_all = "camelCase")]
pub struct GeoRock {
    pub hits_left: u8,
    pub id: String,
    pub scene_name: String,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(deny_unknown_fields, rename_all = "camelCase")]
pub struct PersistentBoolItem {
    pub activated: bool,
    pub semi_persistent: bool,
    pub id: String,
    pub scene_name: String,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(deny_unknown_fields, rename_all = "camelCase")]
pub struct PersistentIntItem {
    pub value: i32,
    pub semi_persistent: bool,
    pub id: String,
    pub scene_name: String,
}
