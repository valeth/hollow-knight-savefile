use std::{env, fs::OpenOptions};
use serde_json as json;
use hollow_knight_savefile::{Result, read_savefile};

fn main() -> Result<()> {
    let args = env::args().skip(1).take(2).collect::<Vec<_>>();
    let infile = &args[0];
    let outfile = &args[1];
    let json = read_savefile(infile)?;
    let json_file = OpenOptions::new().create(true).truncate(true).write(true).open(outfile)?;
    json::to_writer_pretty(json_file, &json)?;

    Ok(())
}
