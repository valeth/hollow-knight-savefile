use std::{fs::File, io::Read, path::Path};
use aes::Aes256;
use block_modes::{Ecb, BlockMode, block_padding::Pkcs7};
use serde_json as json;

mod data;
pub use data::SaveData;

pub type Result<T, E = Box<dyn std::error::Error>> = std::result::Result<T, E>;
type Aes256Ecb = Ecb<Aes256, Pkcs7>;

const CSHARP_HEADER: [u8; 22] = [0, 1, 0, 0, 0, 255, 255, 255, 255, 1, 0, 0, 0, 0, 0, 0, 0, 6, 1, 0, 0, 0];
const AES_KEY: &str = "UKu52ePUBwetZ9wNX88o54dnfKRu0T1l";

pub fn read_savefile<P: AsRef<Path>>(path: P) -> Result<SaveData> {
    let mut file = File::open(path)?;
    let mut data= Vec::new();
    file.read_to_end(&mut data)?;
    let data = data;
    convert_savedata(&data)
}

fn convert_savedata(data: &[u8]) -> Result<SaveData> {
    let body_base64= remove_headers(&data);
    let ciphertext= base64::decode(body_base64)?;
    let plaintext = decrypt(&ciphertext)?;
    Ok(json::from_str(&plaintext)?)
}

fn remove_headers(input: &[u8]) -> &[u8] {
    let (_, body) = input
        .strip_prefix(&CSHARP_HEADER).expect("No CSharp header")
        .split_last().unwrap();

    let mut idx = 0;
    for elem in body.iter().take(5) {
        idx += 1;
        if (elem & 0x80) == 0 { break }
    }

    let (_, body) = body.split_at(idx);

    body
}

fn decrypt(input: &[u8]) -> Result<String> {
    let cipher = Aes256Ecb::new_from_slices(AES_KEY.as_bytes(), &[])?;
    let data = cipher.decrypt_vec(&input)?;
    Ok(String::from_utf8(data)?)
}
